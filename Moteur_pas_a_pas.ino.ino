/*
  This is a test sketch for the Adafruit assembled Motor Shield for Arduino v2
  It won't work with v1.x motor shields! Only for the v2's with built in PWM
  control

  For use with the Adafruit Motor Shield v2
  ---->	http://www.adafruit.com/products/1438
*/


#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61);

// Connect a stepper motor with 513 steps per revolution (xx degree)
// to motor port #2 (M3 and M4)
Adafruit_StepperMotor *myMotor = AFMS.getStepper(513, 2);



/*
   Wiring stepper 28 BYJ-48 5V
   bleue: M4 top
   pink: M3 bottom
   red : GND
   orange: M3 top
   yellow : M4 bottom
   On the mega, the top and bottom refer to the position of the connection when the user can read the text on the shield.
*/
void setup() {
  Wire.begin(); // join i2c bus (address optional for master)
  Serial.begin(230400);           // set up Serial library at 9600 bps
  Serial.println("Stepper test!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz

  myMotor->setSpeed(1);  // 5 rpm
}

void loop() {
  // one rotation = 692 steps at 5 rpm, double
  myMotor->step(962 , FORWARD,  DOUBLE);

  // myMotor->step(1, FORWARD,  DOUBLE);
  // delay(1000);
  //test3();


  Wire.beginTransmission(42); // transmit to device #8
  Wire.write(1);              // sends one byte
  Wire.endTransmission();    // stop transmitting
  delay(300);

}

void test3() {
  // Turn in one direction
  Serial.println("Double coil steps");
  myMotor->step(5000, FORWARD, DOUBLE);
}


void test2() {

  Serial.println("Single coil steps");
  myMotor->step(1000, FORWARD, SINGLE);
  myMotor->step(1000, BACKWARD, SINGLE);

  Serial.println("Double coil steps");
  myMotor->step(1000, FORWARD, DOUBLE);
  myMotor->step(1000, BACKWARD, DOUBLE);
}


void test() {

  Serial.println("Single coil steps");
  myMotor->step(100, FORWARD, SINGLE);
  myMotor->step(100, BACKWARD, SINGLE);

  Serial.println("Double coil steps");
  myMotor->step(100, FORWARD, DOUBLE);
  myMotor->step(100, BACKWARD, DOUBLE);

  Serial.println("Interleave coil steps");
  myMotor->step(100, FORWARD, INTERLEAVE);
  myMotor->step(100, BACKWARD, INTERLEAVE);

  Serial.println("Microstep steps");
  myMotor->step(50, FORWARD, MICROSTEP);
  myMotor->step(50, BACKWARD, MICROSTEP);
}

